"""
Get quickly a subtree without having to parse the full newick.
"""

# This cython version is 4x faster than a similar python-only
# version. Most of the speedup comes from the memoryview.


def node_ids(nw_str, unsigned int ignore_quotes=False):
    """Walk the given newick tree and yield the node ids, their starting
    positions, if they are leaves, and if they were seen before."""
    assert nw_str.endswith(';')

    cdef const unsigned char[:] nw = memoryview(nw_str.encode('utf8'))
    cdef unsigned char c
    cdef unsigned long pos
    cdef list node_id

    pos = 0  # current position in the newick that we are traversing
    node_id = []  # id of the current node
    changed_node = True  # did we change node with the last character?
    seen = False  # has the node (that we yield) been seen before?
    in_quote = False  # are we inside quoted text? (text between " or ')

    try:
        while pos < len(nw):
            c = nw[pos]  # character at the current position

            if c in b' \t\n\r':  # skip spaces
                pos += 1
                continue

            if changed_node:
                is_leaf = not seen and c != b'('
                yield tuple(node_id), pos, is_leaf, seen

            if not ignore_quotes and c in [b'"', b"'"]:
                in_quote = not in_quote

            if in_quote or c not in b'(,)':  # the chars that mark a new node
                changed_node = False
                pos += 1
                continue

            if c == b'(':
                node_id.append(0)
                seen = False
            elif c == b',':
                node_id[-1] += 1
                seen = False
            elif c == b')':
                node_id.pop()
                seen = True

            changed_node = True
            pos += 1
    except IndexError:
        xtra = '' if ignore_quotes else ' Maybe use ignore_quotes=True ?'
        raise ValueError('Newick looks malformed.' + xtra)

    assert not in_quote, ('Newick seems to incorrectly use quotes '
                          '(" and/or \'). Maybe use ignore_quotes=True ?')


def get_subtree(nw, node_id, ignore_quotes=False):
    """Return newick corresponding to the subtree identified by node_id."""
    node_ids_generator = node_ids(nw, ignore_quotes)

    try:
        start, is_leaf = next((pos, is_leaf)
                              for nid, pos, is_leaf, _ in node_ids_generator
                              if nid == node_id)
        if is_leaf:
            _, end, _, _ = next(node_ids_generator)
        else:
            end = next(pos for nid, pos, _, _ in node_ids_generator
                       if nid == node_id) + 1
            if node_id == ():  # a bit hackish, should not be a special case
                end = len(nw)
            else:
                _, end, _, _ = next(node_ids_generator)

        return nw[start:end-1] + ';'
    except StopIteration as e:
        raise ValueError(f'No node found with id {node_id}')


def prune(nw, depth=1, ignore_quotes=False):
    """Return newick of the given tree pruned at the given depth."""
    excluded = []  # will contain the excluded regions
    node_ids_generator = node_ids(nw, ignore_quotes)
    for node_id, pos, _, _ in node_ids_generator:
        start = pos
        while len(node_id) > depth:  # skip nodes at higher depths
            node_id, pos, _, _ = next(node_ids_generator)
        if pos > start:
            excluded.append( (start-1, pos) )

    included = [ (0, len(nw)) ]  # will contain the included regions
    for e1, e2 in excluded:
        i1, i2 = included.pop()
        included.extend([(i1, e1), (e2, i2)])

    return ''.join(nw[i1:i2] for i1, i2 in included)


def info(nw, ignore_quotes=False):
    """Return the number of nodes and leaves and maximum depth of the newick."""
    cdef unsigned int nnodes, nleaves, max_depth

    nnodes, nleaves, max_depth = 0, 0, 0
    for node_id, _, is_leaf, seen in node_ids(nw, ignore_quotes):
        if not seen:
            max_depth = max(max_depth, len(node_id))
            nnodes += 1
            if is_leaf:
                nleaves += 1

    return nnodes, nleaves, max_depth


def get_structure(nw, ignore_quotes=False):
    """Return dict {node_id: (#descendants, #children, start, end) }."""
    info = {}

    for nid, pos, is_leaf, seen in node_ids(nw, ignore_quotes):
        if not seen:  # the first time we traverse this node
            info[nid] = (0, 0, pos, 0)  # initialize numbers and starting position

        if is_leaf or seen:  # the last time we traverse this node
            ndesc, nch, start, end = info[nid]
            info[nid] = (ndesc, nch, start, pos)  # update position where node ends

            if len(nid) > 0:  # update parent (except for the root)
                pnid = nid[:-1]
                pndesc, pnch, pstart, pend = info[pnid]  # parent values

                info[pnid] = (pndesc + ndesc + 1, pnch + 1, pstart, pend)

    return info
