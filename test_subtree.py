# To run with pytest.

import pytest

import subtree

good_trees = """\
;
a;
(a);
(a,b);
(,(dfd)gg);
((B:0.2,(C:0.3,D:0.4)E:0.5)A:0.1)F;
(,,(,));
(A,B,(C,D));
(A, (B, C), (D, E));
(A,B,(C,D)E)F;
(:0.1,:0.2,(:0.3,:0.4):0.5);
(:0.1,:0.2,(:0.3,:0.4):0.5):0.6;
(A:0.1,B:0.2,(C:0.3,D:0.4):0.5);
(A:0.1,B:0.2,(C:0.3,D:0.4)E:0.5)F;
((B:0.2,(C:0.3,D:0.4)E:0.5)A:0.1)F;
([&&NHX:p1=v1:p2=v2],c);
((G001575.1:0.243,G002335.1:0.2)42:0.041,G001615.1:0.246)'100.0:d__Bacteria';
(a,(b)'the_answer_is_''yes''');
(((One:0.2,Two:0.3):0.3,(Three:0.5,Four:0.3):0.2):0.3,Five:0.7):0;
((C,D)1,(A,(B,X)3)2,E)R;
([]);
((C,D)[1],(A,(B,X)[3])[2],E)[R];
( a a : 3[comment here] [comment there]);
( 'a a' : 3[comment here] [comment there]);
('Sp aeria' et.:0[&&NHX:taxid=1500313:name='Sp aeria' et.]);
('this is mean: text ''with'' (well, [1, 2...] or more) tricky characters':1);
("this is mean too: text ""with"" (well, [1, 2...] or more) tricky chars":2);
""".splitlines()

need_quoting = """\
('this is mean text ''with'' well, ))? [1, 2...] or more tricky characters':1);
("this is mean too: text ""with"" ()), tricky chars":2);
""".splitlines()

need_not_quoting = """\
(Sanderson's spider:3,Say what you want);
((Species1:4,Sanderson's spider:3),(Someone's pet, Someone else's unicorn));
""".splitlines()


def test_node_ids():
    for nw in good_trees:
        for node_id, pos, is_leaf, seen in subtree.node_ids(nw):
            pass  # no exceptions

    for nw in need_quoting:
        for data in subtree.node_ids(nw, ignore_quotes=False):
            pass  # no exceptions

        with pytest.raises(ValueError):
            for data in subtree.node_ids(nw, ignore_quotes=True):
                pass

    for nw in need_not_quoting:
        for data in subtree.node_ids(nw, ignore_quotes=True):
            pass  # no exceptions

        with pytest.raises(AssertionError):
            for data in subtree.node_ids(nw, ignore_quotes=False):
                pass

    nw = '((t1,t2)ax,(t3,t4)bx)cx;'
    assert ([(node_id, pos, is_leaf, seen)
             for node_id, pos, is_leaf, seen in subtree.node_ids(nw)] ==
            [((), 0, False, False),
             ((0,), 1, False, False),
             ((0, 0), 2, True, False),
             ((0, 1), 5, True, False),
             ((0,), 8, False, True),
             ((1,), 11, False, False),
             ((1, 0), 12, True, False),
             ((1, 1), 15, True, False),
             ((1,), 18, False, True),
             ((), 21, False, True)])


def test_get_subtree():
    for i, nw in enumerate(good_trees[5:8]):
        # TODO
        # node_id = [0, 1]
        # assert get_subtree(nw, node_id) == expected[i]
        pass


def test_prune():
    expected = ['(A:0.1)F;', '(,,);', '(A,B,);']
    for i, nw in enumerate(good_trees[5:8]):
        assert subtree.prune(nw, 1) == expected[i]


def test_info():
    expected = [
        (1, 1, 0),
        (1, 1, 0),
        (2, 1, 1),
        (3, 2, 1),
        (4, 2, 2),
        (6, 3, 3),
        (6, 4, 2),
        (6, 4, 2)]
    for i, nw in enumerate(good_trees[:8]):
        assert subtree.info(nw) == expected[i]
