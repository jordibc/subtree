# To compile the cython module without setup.py or anything.

py_version := $(shell python -V | egrep -o '3\.[0-9]+')

subtree.so: subtree.pyx
	cython3 -3 subtree.pyx
	gcc -fPIC -shared -o subtree.so subtree.c \
		-I/usr/include/python${py_version} -lpython${py_version}

clean:
	rm -rf __pycache__ subtree.so subtree.c
