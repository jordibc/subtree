#!/usr/bin/env python3

"""
Quickly do some simple operations on a newick without parsing it.

Things like showing some basic information, getting a subtree, and
prunning to a given depth.
"""

import sys
import pprint
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter as fmt

import subtree


def main():
    args = get_args()

    try:
        assert args.prune is None or args.prune > 0, 'Prune depth must be > 0'

        # Get the newick string. It will be overwritten later on.
        nw = (open(args.FILE) if args.FILE != '-' else sys.stdin).read().strip()

        # Get the subtree identified by node_id, if requested.
        if args.node_id:
            node_id = tuple(int(x) for x in args.node_id.split(','))
            nw = subtree.get_subtree(nw, node_id, args.ignore_quotes)

        # Prune the (sub)tree, if requested.
        if args.prune is not None:
            nw = subtree.prune(nw, args.prune, args.ignore_quotes)

        # Output as a newick, or summary information, etc.
        if args.output == 'newick':
            print(nw)
        elif args.output == 'info':  # show number of nodes, leaves, and max depth
            nnodes, nleaves, max_depth = subtree.info(nw, args.ignore_quotes)
            print('Number of nodes:  %8d' % nnodes)
            print('Number of leaves: %8d' % nleaves)
            print('Maximum depth:    %8d' % max_depth)
        elif args.output == 'structure':  # show structure
            structure = subtree.get_structure(nw, args.ignore_quotes)
            print('Structure {node_id: (#descendants, #children, start, end) }')
            pprint.pprint(structure)
        elif args.output == 'debug':  # show all subtrees, useful for debugging
            assert len(nw) < 150, f'Newick too big ({len(nw)} bytes).'
            print_all_node_ids(nw, args.ignore_quotes)
        else:
            raise ValueError(f'Unknown output: {args.output}')
    except (FileNotFoundError, ValueError, AssertionError) as e:
        sys.exit(e)


def get_args():
    parser = ArgumentParser(description=__doc__, formatter_class=fmt)
    add = parser.add_argument  # shortcut

    add('FILE', help='file with the newick tree representation (- for stdin)')
    add('-n', '--node-id', help='id of the node that we want to read')
    add('-p', '--prune', metavar='DEPTH', type=int, help='prune tree to the given depth')
    add('-o', '--output', choices=['newick', 'info', 'structure', 'debug'],
        default='newick',
        help=('"newick" for the resulting tree; "info" for the number of '
              'nodes, etc.; "structure" for structure information; '
              '"debug" to show all subtrees'))
    add('-q', '--ignore-quotes', action='store_true', help='ignore quoted text in newick')

    return parser.parse_args()


def print_all_node_ids(nw, ignore_quotes=False):
    """For all the nodes in the tree, print info when changing node_ids."""
    print(f'Newick:\n{nw}\n')

    print('start/end marks '.ljust(len(nw)+1) + 'node id       subtree')
    for node_id, pos, is_leaf, _ in subtree.node_ids(nw, ignore_quotes):
        mark = nw[pos]
        print(' '*pos + mark + ' '*(len(nw)-pos) + str(node_id).ljust(13),
              subtree.get_subtree(nw, node_id, ignore_quotes))



if __name__ == '__main__':
    main()
