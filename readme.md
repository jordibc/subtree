# Subtree

Experiments on the use of [Cython](https://cython.org/) and
[memoryviews](https://docs.python.org/3/c-api/memoryview.html) to
extract faster information from newicks.

The `subtree` library iterates over the nodes in the newick tree very
fast, and the `get_subree.py` command demonstrates how to use it to
access a subtree from a newick by using its "node id".


## 📥 Installation

You can download this repository and use the `get_subtree.py` program
directly from this directory.

It will use `subtree.py` as a module. But if you have Cython and do:

```sh
$ make
```

it will compile a faster version from `subtree.pyx`.

Actually, one of the main reasons to develop this was to compare the
performance of the compiled module versus the pure python one.


## 💡 Examples

Getting a subtree (giving the node id with `--node-id <id>`):

```sh
$ ./get_subtree.py GTDB_bact_r95.tree -n 1,1,1,1,0,0
((O2-12-FULL-45-9-A sp001771365:0.208,GCA-2774365 sp002774365:0.247)O2-12-FULL-45-9:0.04,XYD2-FULL-41-8 sp001771545:0.231)O2-12-FULL-45-9:0.082
```

Prunning the tree (with `--prune <depth>`):

```sh
$ ./get_subtree.py GTDB_bact_r95.tree -p 2
((Bacteria:0.021,Bacteria:0.024)Bacteria:0.02,(Bacteria:0.057,Bacteria:0.052)Bacteria:0.02);
```

Showing some information about a tree (with `--output info`):

```sh
$ ./get_subtree.py GTDB_bact_r95.tree -o info
Number of nodes:   60475
Number of leaves:  30238
Maximum depth:     83
```

Showing the structure of the tree (with `--output structure`):

```sh
$ echo '((a,b):3,(node1,d));' | ./get_subtree.py - -o structure
Structure {node_id: (#descendants, #children, start, end) }
{(): (6, 2, 0, 19),
 (0,): (2, 2, 1, 6),
 (0, 0): (0, 0, 2, 2),
 (0, 1): (0, 0, 4, 4),
 (1,): (2, 2, 9, 18),
 (1, 0): (0, 0, 10, 10),
 (1, 1): (0, 0, 16, 16)}
```

Seeing all the subtrees found by the library (with `--output debug`):

```sh
$ echo '((a,b):3,(node1,d));' | ./get_subtree.py - -o debug
Newick:
((a,b):3,(node1,d));

start/end marks      node id       subtree
(                    []            ((a,b):3,(node1,d))
 (                   [0]           (a,b):3
  a                  [0, 0]        a
    b                [0, 1]        b
      :              [0]           (a,b):3
         (           [1]           (node1,d)
          n          [1, 0]        node1
                d    [1, 1]        d
                  )  [1]           (node1,d)
                   ; []            ((a,b):3,(node1,d))
```


## ⏱️ Tests

You can run the tests in `test_subtree.py` with:

```sh
$ pytest
```
